package com.vuongdt23.currencyapp.repositories;

import java.util.List;

import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.vuongdt23.currencyapp.entities.CurrencyMetadata;

public interface CurrencyMetadataRepository extends JpaRepository<CurrencyMetadata, UUID> {

    List<CurrencyMetadata> findByIsDeletedFalse(Pageable pageable);
}