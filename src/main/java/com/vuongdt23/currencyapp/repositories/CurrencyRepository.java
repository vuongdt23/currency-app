package com.vuongdt23.currencyapp.repositories;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vuongdt23.currencyapp.entities.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, UUID> {
    Optional<Currency> findByIdAndIsDeletedFalse(UUID id);

    Page<Currency> findByIsDeleted(boolean isDeleted, Pageable pageable);

    Optional<Currency> findByCodeAndIsDeletedFalse(String code);

    @Query("SELECT CASE WHEN COUNT(e) > 0 THEN true ELSE false END FROM Currency e WHERE e.code = :code AND e.isDeleted = false")
    boolean existsByCodeAndDeletedFalse(@Param("code") String code);

    @Query("SELECT COUNT(e) > 0 FROM Currency e WHERE e.code = :code AND e.isDeleted = false AND e.id <> :id")
    boolean existsByCodeAndIsDeletedFalseAndIdNot(@Param("code") String code, @Param("id") UUID id);

    @Query("SELECT e FROM Currency e WHERE e.metadataId = :metadataId AND e.isDeleted = false")
    Set<Currency> findByMetadataIdAndIsDeletedFalse(UUID metadataId);

}
