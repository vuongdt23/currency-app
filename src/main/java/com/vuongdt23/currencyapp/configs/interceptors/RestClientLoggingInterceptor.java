package com.vuongdt23.currencyapp.configs.interceptors;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

public class RestClientLoggingInterceptor implements ClientHttpRequestInterceptor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        logRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        logResponse(response);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) throws IOException {

        String requestBody = new String(body, StandardCharsets.UTF_8);
        log.info("Out going request to External API: ");
        log.info("URI: {}, Method: {}, Headers: {}, Request body: {}", request.getURI(), request.getMethod(), request.getHeaders(), requestBody);

    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        InputStream responseBodyStream = response.getBody();
        String responseBody = StreamUtils.copyToString(responseBodyStream, StandardCharsets.UTF_8);
        int statusCode = response.getStatusCode().value();
        
        log.info("Status code: {}, Status text: {}, Headers: {}, Response body: {}", statusCode, response.getStatusText(), response.getHeaders(), responseBody);

    }
}
