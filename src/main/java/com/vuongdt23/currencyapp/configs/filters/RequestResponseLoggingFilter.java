package com.vuongdt23.currencyapp.configs.filters;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class RequestResponseLoggingFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(RequestResponseLoggingFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper((HttpServletRequest) request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(
                (HttpServletResponse) response);
        try {
            chain.doFilter(requestWrapper, responseWrapper);
        } finally {
            String requestBody = new String(requestWrapper.getContentAsByteArray(),
                    requestWrapper.getCharacterEncoding());
            String responseBody = new String(responseWrapper.getContentAsByteArray(),
                    responseWrapper.getCharacterEncoding());
            int statusCode = responseWrapper.getStatus();
            final String ip = requestWrapper.getRemoteAddr();
            final String requestUrl = requestWrapper.getRequestURL().toString();
            final String clientId = requestWrapper.getHeader("any-header");
            responseWrapper.copyBodyToResponse();
            
            log.info("Request URL: {}, IP: {}, Client ID: {}, Status Code: {}, Request Body: {}, Response Body: {}",
                    requestUrl, ip, clientId, statusCode, requestBody, responseBody);
        }
    }
}
