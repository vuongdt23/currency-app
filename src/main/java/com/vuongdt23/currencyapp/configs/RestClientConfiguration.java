package com.vuongdt23.currencyapp.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClient;

import com.vuongdt23.currencyapp.configs.interceptors.RestClientLoggingInterceptor;

@Configuration
public class RestClientConfiguration {

    @Bean
    RestClient defaultClient() {

        RestClient client = RestClient.builder().requestInterceptor(new RestClientLoggingInterceptor())
                .requestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory())).build();

        return client;
    };

}
