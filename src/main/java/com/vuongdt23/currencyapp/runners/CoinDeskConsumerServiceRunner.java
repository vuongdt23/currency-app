package com.vuongdt23.currencyapp.runners;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vuongdt23.currencyapp.dto.external.CoinDeskDTO;
import com.vuongdt23.currencyapp.repositories.CurrencyRepository;
import com.vuongdt23.currencyapp.repositories.CurrencyMetadataRepository;

import com.vuongdt23.currencyapp.entities.Currency;
import com.vuongdt23.currencyapp.entities.CurrencyMetadata;;

@Component
@EnableScheduling
public class CoinDeskConsumerServiceRunner implements ApplicationRunner {

    private static Logger log = LoggerFactory.getLogger(CoinDeskConsumerServiceRunner.class.getSimpleName());

    @Value("${coindesk.uri}")
    private String coindeskUri;

    @Value("${retry.maxAttempts}")
    private int maxAttempts;

    @Value("${retry.delay}")
    private long delay;

    private CurrencyRepository currencyRepository;
    private CurrencyMetadataRepository currencyMetadataRepository;
    private RestClient restClient;

    @Autowired
    public void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Autowired
    public void CurrencyMetadataRepository(CurrencyMetadataRepository currencyMetadataRepository) {
        this.currencyMetadataRepository = currencyMetadataRepository;
    }

    @Autowired
    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        synchronizeCurrencyData();
    }

    @Scheduled(cron = "0 0 * * * *")
    @Retryable(value = {
            Exception.class }, maxAttemptsExpression = "#{${retry.maxAttempts}}", backoff = @Backoff(delayExpression = "#{${retry.delay}}"))
    private void synchronizeCurrencyData() {
        try {
            CoinDeskDTO coinDeskDTO = restClient.get().uri(coindeskUri)
                    .retrieve().body(CoinDeskDTO.class);

            saveDataFromAPI(coinDeskDTO);
        } catch (Exception e) {
            log.error("External Service Call err", e);
        }
    }

    @Recover
    private void recoverFromException(Exception e) {
        log.error("Failed to retrieve data from external API. Attempting to recover from backup...");
        try {
            CoinDeskDTO coinDeskDTO = readFromJsonFile();
            saveDataFromAPI(coinDeskDTO);

        } catch (Exception ex) {
            log.error("Failed to recover data from backup.", ex);
        }
    }

    private void saveDataFromAPI(CoinDeskDTO coinDeskDTO) {
        CurrencyMetadata currencyMetadata = CurrencyMetadata.builder()
                .chartName(coinDeskDTO.getChartName())
                .disclaimer(coinDeskDTO.getDisclaimer())
                .updateTime(coinDeskDTO.getTime().getUpdated())
                .updateTimeISO(coinDeskDTO.getTime().getUpdatedISO())
                .updateTimeUK(coinDeskDTO.getTime().getUpdateduk())
                .isDeleted(false)
                .build();

        currencyMetadataRepository.save(currencyMetadata);

        Set<Currency> currencies = coinDeskDTO.getBpi().entrySet().stream().map(data -> {
            Currency newCurrency = Currency.builder()
                    .code(data.getValue().getCode())
                    .name(data.getKey())
                    .description(data.getValue().getDescription())
                    .symbol(data.getValue().getSymbol())
                    .rate(data.getValue().getRate_float())
                    .isDeleted(false)
                    .build();
            newCurrency.setMetadataId(currencyMetadata.getId());

            return newCurrency;

        }).collect(Collectors.toSet());

        for (Currency currency : currencies) {
            // Check if currency exists in database
            Optional<Currency> optionalCurrency = currencyRepository
                    .findByCodeAndIsDeletedFalse(currency.getCode());
            if (optionalCurrency.isPresent()) {
                Currency existingCurrency = optionalCurrency.get();
                existingCurrency.setSymbol(currency.getSymbol());
                existingCurrency.setRate(currency.getRate());
                existingCurrency.setDescription(currency.getDescription());
                existingCurrency.setDeleted(currency.isDeleted());
                existingCurrency.setMetadataId(currencyMetadata.getId());
                currencyRepository.save(existingCurrency);
            } else {
                // If currency does not exist, save the new currency object
                currency.setMetadataId(currencyMetadata.getId());
                currencyRepository.save(currency);
            }
        }
    }

    private CoinDeskDTO readFromJsonFile() throws IOException {
        Resource resource = new ClassPathResource("backup/coindeskAPISnapshot.json");
        if (resource.exists()) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(resource.getFile(), CoinDeskDTO.class);
        } else {
            throw new IOException("Backup JSON file not found");
        }
    }

}
