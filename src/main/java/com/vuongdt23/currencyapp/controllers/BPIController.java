package com.vuongdt23.currencyapp.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vuongdt23.currencyapp.dto.BPIResponse;
import com.vuongdt23.currencyapp.services.CurrencyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("/api/bpi")
public class BPIController {

    private CurrencyService currencyService;

    @Autowired
    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping("/currentprice")
    public ResponseEntity<BPIResponse> getBPIData() {
        return ResponseEntity.ok(currencyService.getBPIData());
    }

}
