package com.vuongdt23.currencyapp.controllers;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vuongdt23.currencyapp.dto.CurrencyDTO;
import com.vuongdt23.currencyapp.dto.CurrencyRequest;
import com.vuongdt23.currencyapp.services.CurrencyService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/currencies")
public class CurrencyController {

    private CurrencyService currencyService;

    @Autowired
    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping
    public ResponseEntity<List<CurrencyDTO>> getAllCurrencies(
            @RequestParam(required = false, defaultValue = "10") int pageSize,
            @RequestParam(required = false, defaultValue = "0") int pageNumber) {
        return ResponseEntity.ok(currencyService.getAllCurrenciesWithPagination(pageNumber, pageSize).getContent());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CurrencyDTO> getCurrencyById(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(currencyService.getCurrencyById(id));
    }

    @PostMapping
    public ResponseEntity<CurrencyDTO> createCurrency(@RequestBody @Valid CurrencyRequest data) {

        return new ResponseEntity<>(currencyService.createCurrency(data), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CurrencyDTO> updateCurrency(
            @PathVariable("id") UUID id,
            @RequestBody @Valid CurrencyRequest data) {
        return ResponseEntity.ok(currencyService.updateCurrency(data, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCurrency(@PathVariable("id") UUID id) {
        currencyService.deleteCurrency(id);
        return ResponseEntity.noContent().build();

    }
}
