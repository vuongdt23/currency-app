package com.vuongdt23.currencyapp.entities;

import java.time.Instant;
import java.util.UUID;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
@Table(name = "CURRENCY")
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "CURRENCY_ID")
    private UUID id;

    @Column(name = "CURRENCY_CODE")
    private String code;

    @Column(name = "CURRENCY_NAME")
    private String name;

    @Column(name = "CURRENCY_SYMBOL")
    private String symbol;

    @Column(name = "CURRENCY_RATE")
    private double rate;

    @Column(name = "CURRENCY_DESCRIPTION")
    private String description;

    @Column(name = "CURRENCY_IS_DELETED")
    private boolean isDeleted;

    @Column(name = "CURRENCY_METADATA_ID")
    private UUID metadataId;

    @Column(name = "CURRENCY_CREATED_AT")
    @CreationTimestamp
    private Instant createdAt;

    @Column(name = "CURRENCY_UPDATED_AT")
    @UpdateTimestamp
    private Instant updatedAt;

}
