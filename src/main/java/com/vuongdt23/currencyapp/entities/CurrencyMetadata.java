package com.vuongdt23.currencyapp.entities;

import java.util.UUID;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CURRENCY_METADATA")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyMetadata {

    @Id
    @GeneratedValue
    @Column(name = "CURRENCY_METADATA_ID")
    private UUID id;

    @Column(name = "CURRENCY_METADATA_UPDATE_TIME")
    private String updateTime;

    @Column(name = "CURRENCY_METADATA_UPDATE_TIME_ISO")
    private String updateTimeISO;

    @Column(name = "CURRENCY_METADATA_UPDATE_TIME_UK")
    private String updateTimeUK;

    @Column(name = "CURRENCY_METADATA_DISCLAIMER")
    private String disclaimer;

    @Column(name = "CURRENCY_METADATA_CHART_NAME")
    private String chartName;

    @Column(name = "CURRENCY_METADATA_IS_DELETED")
    private boolean isDeleted;

    @CreationTimestamp
    @Column(name = "CURRENCY_METADATA_CREATED_AT")
    private String createdAt;

    @UpdateTimestamp
    @Column(name = "CURRENCY_METADATA_UPDATED_AT")
    private String updatedAt;
}