package com.vuongdt23.currencyapp.services.implementations;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.vuongdt23.currencyapp.dto.BPIResponse;
import com.vuongdt23.currencyapp.dto.BPIResponse.BPICurrencyDTO;
import com.vuongdt23.currencyapp.dto.CurrencyDTO;
import com.vuongdt23.currencyapp.dto.CurrencyRequest;
import com.vuongdt23.currencyapp.entities.Currency;
import com.vuongdt23.currencyapp.entities.CurrencyMetadata;
import com.vuongdt23.currencyapp.repositories.CurrencyMetadataRepository;
import com.vuongdt23.currencyapp.repositories.CurrencyRepository;
import com.vuongdt23.currencyapp.services.CurrencyService;

import jakarta.transaction.Transactional;

@Service
public class CurrencyServiceImplementation implements CurrencyService {

    private CurrencyRepository currencyRepository;

    private CurrencyMetadataRepository currencyMetadataRepository;
    private static Logger log = LoggerFactory.getLogger(CurrencyServiceImplementation.class.getSimpleName());

    @Autowired
    public void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;

    }

    @Autowired
    public void setCurrencyMetadataRepository(CurrencyMetadataRepository currencyMetadataRepository) {
        this.currencyMetadataRepository = currencyMetadataRepository;

    }

    @Override
    public Page<CurrencyDTO> getAllCurrenciesWithPagination(int pageNumber, int pageSize) {
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("code").ascending());
        return currencyRepository.findByIsDeleted(false, pageRequest).map(currency -> {
            return CurrencyDTO.builder().id(currency.getId())
                    .description(currency.getDescription())
                    .code(currency.getCode())
                    .createdAt(currency.getCreatedAt())
                    .name(currency.getName())
                    .updatedAt(currency.getUpdatedAt())
                    .symbol(currency.getSymbol()).rateFloat(currency.getRate())
                    .rate(String.valueOf(currency.getRate()))
                    .build();
        });
    }

    @Override
    public CurrencyDTO getCurrencyById(UUID id) {
        try {
            Currency currency = currencyRepository.findByIdAndIsDeletedFalse(id).orElseThrow(() -> {
                return new ResponseStatusException(HttpStatus.NOT_FOUND);
            });

            return CurrencyDTO.builder().id(currency.getId())
                    .description(currency.getDescription())
                    .code(currency.getCode())
                    .createdAt(currency.getCreatedAt())
                    .name(currency.getName())
                    .updatedAt(currency.getUpdatedAt())
                    .symbol(currency.getSymbol()).rateFloat(currency.getRate())
                    .rate(String.valueOf(currency.getRate()))
                    .build();
        } catch (ResponseStatusException responseStatusException) {
            throw responseStatusException;
        } catch (Exception e) {
            log.error("err", e);
            throw e;
        }

    }

    @Override
    public CurrencyDTO createCurrency(CurrencyRequest data) {
        try {
            if (currencyRepository.existsByCodeAndDeletedFalse(data.getCode())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            Currency newCurrency = Currency.builder()
                    .code(data.getCode())
                    .name(data.getName())
                    .description(data.getDescription())
                    .symbol(data.getSymbol())
                    .rate(data.getRate())
                    .isDeleted(false)
                    .build();
            currencyRepository.save(newCurrency);
            return CurrencyDTO.builder().id(newCurrency.getId())
                    .description(newCurrency.getDescription())
                    .code(newCurrency.getCode())
                    .name(newCurrency.getName())
                    .createdAt(newCurrency.getCreatedAt())
                    .updatedAt(newCurrency.getUpdatedAt())
                    .symbol(newCurrency.getSymbol()).rateFloat(newCurrency.getRate())
                    .rate(String.valueOf(newCurrency.getRate()))
                    .build();
        } catch (ResponseStatusException responseStatusException) {
            throw responseStatusException;
        } catch (Exception e) {

            log.error("err", e);
            throw e;
        }
    }

    @Override
    @Transactional
    public CurrencyDTO updateCurrency(CurrencyRequest data, UUID id) {
        try {
            Currency currency = currencyRepository.findByIdAndIsDeletedFalse(id).orElseThrow(() -> {
                return new ResponseStatusException(HttpStatus.NOT_FOUND);
            });

            if (currencyRepository.existsByCodeAndIsDeletedFalseAndIdNot(data.getCode(), id)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            currency.setName(data.getName());
            currency.setCode(data.getCode());
            currency.setDescription(data.getDescription());
            currency.setRate(data.getRate());
            currency.setSymbol(data.getSymbol());

            currencyRepository.save(currency);

            return CurrencyDTO.builder().id(currency.getId())
                    .description(currency.getDescription())
                    .code(currency.getCode())
                    .createdAt(currency.getCreatedAt())
                    .updatedAt(currency.getUpdatedAt())
                    .name(currency.getName())
                    .symbol(currency.getSymbol()).rateFloat(currency.getRate())
                    .rate(String.valueOf(currency.getRate()))
                    .build();

        } catch (ResponseStatusException responseStatusException) {
            throw responseStatusException;
        } catch (Exception e) {
            log.error("err", e);

            throw e;
        }
    }

    @Override
    @Transactional
    public CurrencyDTO deleteCurrency(UUID id) {
        try {
            Currency currency = currencyRepository.findByIdAndIsDeletedFalse(id).orElseThrow(() -> {
                return new ResponseStatusException(HttpStatus.NOT_FOUND);
            });

            currency.setDeleted(true);
            currencyRepository.save(currency);

            return CurrencyDTO.builder().id(currency.getId())
                    .description(currency.getDescription())
                    .code(currency.getCode())
                    .name(currency.getName())
                    .createdAt(currency.getCreatedAt())
                    .updatedAt(currency.getUpdatedAt())
                    .symbol(currency.getSymbol()).rateFloat(currency.getRate())
                    .rate(String.valueOf(currency.getRate()))
                    .build();

        } catch (ResponseStatusException responseStatusException) {
            throw responseStatusException;
        } catch (Exception e) {
            log.error("err", e);

            throw e;
        }
    }

    @Override
    public BPIResponse getBPIData() {
        try {
            Pageable pageRequest = PageRequest.of(0, 1, Sort.by("createdAt").descending());
            List<CurrencyMetadata> metaDataList = currencyMetadataRepository.findByIsDeletedFalse(pageRequest);
            
           if(metaDataList.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
           }
           CurrencyMetadata latestTimeRecord = metaDataList.get(0);
            

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm:ss zzz");
            Instant parsedInstant = Instant.from(formatter.parse(latestTimeRecord.getUpdateTime()));

            BPIResponse bpiResponse = new BPIResponse();
            List<BPICurrencyDTO> currencyResponseData = new ArrayList<>();
            bpiResponse.setUpdateTime(parsedInstant);

            Set<Currency> currencies = currencyRepository.findByMetadataIdAndIsDeletedFalse(latestTimeRecord.getId());

            currencies.stream().forEach(currency -> {
                BPICurrencyDTO currencyData = new BPICurrencyDTO();
                currencyData.setCode(currency.getCode());
                currencyData.setName(currency.getName());
                currencyData.setDescription(currency.getDescription());
                currencyData.setRate(currency.getRate());
                currencyData.setSymbol(currency.getSymbol());
                currencyResponseData.add(currencyData);
            });

            bpiResponse.setBpi(currencyResponseData);
            return bpiResponse;
        } catch (Exception e) {
            log.error("err", e);

            throw e;
        }
    }

}
