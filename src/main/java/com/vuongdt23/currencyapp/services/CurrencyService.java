package com.vuongdt23.currencyapp.services;

import java.util.UUID;

import org.springframework.data.domain.Page;

import com.vuongdt23.currencyapp.dto.BPIResponse;
import com.vuongdt23.currencyapp.dto.CurrencyDTO;
import com.vuongdt23.currencyapp.dto.CurrencyRequest;

public interface CurrencyService {
    public Page<CurrencyDTO> getAllCurrenciesWithPagination(int pageNumber, int pageSize);

    public CurrencyDTO getCurrencyById(UUID id);

    public CurrencyDTO createCurrency(CurrencyRequest data);

    public CurrencyDTO updateCurrency(CurrencyRequest data, UUID id);

    public CurrencyDTO deleteCurrency(UUID id);

    public BPIResponse getBPIData();
}
