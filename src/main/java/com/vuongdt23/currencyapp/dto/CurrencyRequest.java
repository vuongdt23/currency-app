package com.vuongdt23.currencyapp.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CurrencyRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String code;
    @NotBlank
    private String symbol;
    @NotNull
    private double rate;
    @NotBlank
    private String description;
}
