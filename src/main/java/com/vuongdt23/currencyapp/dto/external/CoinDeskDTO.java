package com.vuongdt23.currencyapp.dto.external;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
public class CoinDeskDTO {
    private Time time;
    private String disclaimer;
    private String chartName;
    private Map<String, CoinDeskCurrencyDTO> bpi;

    @Data
    @NoArgsConstructor
    public static class Time {
        private String updated;
        private String updatedISO;
        private String updateduk;
    }

    @Data
    @NoArgsConstructor
    public static class CoinDeskCurrencyDTO {
        private String code;
        private String symbol;
        private String rate;
        private String description;
        private double rate_float;
    }
}
