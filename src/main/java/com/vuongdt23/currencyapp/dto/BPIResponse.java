package com.vuongdt23.currencyapp.dto;

import java.time.Instant;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class BPIResponse {

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "UTC+7")
    private Instant updateTime;

    List<BPICurrencyDTO> bpi;

    @Data
    @NoArgsConstructor
    public static class BPICurrencyDTO {
        private String name;
        private String code;
        private String symbol;
        private double rate;
        private String description;
    }
}
