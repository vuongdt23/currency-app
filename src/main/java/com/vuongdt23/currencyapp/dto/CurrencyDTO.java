package com.vuongdt23.currencyapp.dto;

import java.time.Instant;
import java.util.UUID;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyDTO {

    private UUID id;

    private String code;

    private String symbol;

    private String rate;

    private String name;
    
    private double rateFloat;

    private String description;

    private Instant createdAt;

    private Instant updatedAt;
}
