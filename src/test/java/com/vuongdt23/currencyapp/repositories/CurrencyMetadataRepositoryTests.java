package com.vuongdt23.currencyapp.repositories;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.vuongdt23.currencyapp.entities.CurrencyMetadata;

@DataJpaTest
public class CurrencyMetadataRepositoryTests {
    @Autowired
    private CurrencyMetadataRepository currencyMetadataRepository;


    @Test
    public void givenPresentNotDeletedData_whenfindByNotDeleted_willReturnData(){
        CurrencyMetadata testData = new CurrencyMetadata();

        testData.setDeleted(false);

        currencyMetadataRepository.save(testData);
        Pageable pageable = PageRequest.of(0, 10);
        List<CurrencyMetadata> result = currencyMetadataRepository.findByIsDeletedFalse(pageable );

        Assertions.assertThat(result).contains(testData);
    }

    
    @Test
    public void givenPresentDeletedData_whenfindByNotDeleted_willNotReturnData(){
        CurrencyMetadata testData = new CurrencyMetadata();

        testData.setDeleted(true);

        currencyMetadataRepository.save(testData);
        Pageable pageable = PageRequest.of(0, 10);
        List<CurrencyMetadata> result = currencyMetadataRepository.findByIsDeletedFalse(pageable );

        Assertions.assertThat(result).doesNotContain(testData);
    }

    @Test
    public void givenMultipleValidData_whenfindByNotDeleted_willReturnAllData(){
        CurrencyMetadata testData = new CurrencyMetadata();
        CurrencyMetadata testData2 = new CurrencyMetadata();

        testData.setDeleted(false);
        testData2.setDeleted(false);

        currencyMetadataRepository.save(testData);
        currencyMetadataRepository.save(testData2);

        Pageable pageable = PageRequest.of(0, 10);
        List<CurrencyMetadata> result = currencyMetadataRepository.findByIsDeletedFalse(pageable );

        Assertions.assertThat(result.size()).isEqualTo(2);
    }
}
