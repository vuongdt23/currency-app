package com.vuongdt23.currencyapp.repositories;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.NoSuchElementException;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.vuongdt23.currencyapp.entities.Currency;

@DataJpaTest
public class CurrencyRepositoryTests {
    @Autowired
    private CurrencyRepository currencyRepository;

    @Test
    public void givenNonDeletedCurrency_whenGetById_willReturnCurrency() {
        Currency currency = new Currency();
        currency.setDeleted(false);
        Currency savedCurrency = currencyRepository.save(currency);
        Currency fetchedCurrency = currencyRepository.findByIdAndIsDeletedFalse(currency.getId()).get();
        Assertions.assertThat(savedCurrency.getId()).isEqualTo(fetchedCurrency.getId());
    }

    @Test
    public void givenDeletedCurrency_whenGetById_willNotReturnCurrency() {
        Currency currency = new Currency();
        currency.setDeleted(true);
        currencyRepository.save(currency);
        Assertions.assertThatThrownBy(() -> currencyRepository.findByIdAndIsDeletedFalse(currency.getId()).get())
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void givenNonDeletedCurrency_whenGetByNullId_willNotReturnCurrency() {
        Currency currency = new Currency();
        currency.setDeleted(false);
        currencyRepository.save(currency);
        Assertions.assertThatThrownBy(() -> currencyRepository.findByIdAndIsDeletedFalse(null).get())
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void givenExistingCurrencyAndDifferentId_whenExistsByCodeAndIsDeletedFalseAndIdNot_thenReturnsTrue() {
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID()); //
        currency.setCode("USD"); 
        currency.setDeleted(false);
        currencyRepository.save(currency);

        // Act
        boolean exists = currencyRepository.existsByCodeAndIsDeletedFalseAndIdNot("USD", UUID.randomUUID());
                                                                                                           

        // Assert
        assertTrue(exists);
    }

    @Test
    public void givenExistingCurrencyAndSameId_whenExistsByCodeAndIsDeletedFalseAndIdNot_thenReturnsFalse() {
        Currency currency = new Currency();
        currency.setCode("USD"); 
        currency.setDeleted(false);
        currencyRepository.save(currency);

        boolean exists = currencyRepository.existsByCodeAndIsDeletedFalseAndIdNot("USD", currency.getId()); 

        Assertions.assertThat(exists).isFalse();
    }

    @Test
    public void givenNonExistingCurrency_whenExistsByCodeAndIsDeletedFalseAndIdNot_thenReturnsFalse() {
        boolean exists = currencyRepository.existsByCodeAndIsDeletedFalseAndIdNot("EUR", UUID.randomUUID()); 
        Assertions.assertThat(exists).isFalse();
    }

    @Test
    public void givenDeletedCurrency_whenExistsByCodeAndIsDeletedFalseAndIdNot_thenReturnsFalse() {
        Currency currency = new Currency();
        currency.setId(UUID.randomUUID()); 
        currency.setCode("JPY"); 
        currency.setDeleted(true);
        currencyRepository.save(currency);

        boolean exists = currencyRepository.existsByCodeAndIsDeletedFalseAndIdNot("JPY", UUID.randomUUID()); 

        assertFalse(exists);
    }

}
