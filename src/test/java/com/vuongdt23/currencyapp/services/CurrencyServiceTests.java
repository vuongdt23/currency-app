package com.vuongdt23.currencyapp.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import com.vuongdt23.currencyapp.dto.CurrencyDTO;
import com.vuongdt23.currencyapp.dto.CurrencyRequest;
import com.vuongdt23.currencyapp.entities.Currency;
import com.vuongdt23.currencyapp.repositories.CurrencyRepository;
import com.vuongdt23.currencyapp.services.implementations.CurrencyServiceImplementation;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTests {

    @Mock
    private CurrencyRepository currencyRepository;

    @InjectMocks
    private CurrencyServiceImplementation currencyService;

    @Test
    public void givenCurrencyData_whenCreateCurrency_thenReturnCreatedCurrencyData() {
        CurrencyRequest testData = new CurrencyRequest();
        testData.setName("test");
        when(currencyRepository.save(any(Currency.class))).thenAnswer((invocation) -> {
            Currency entity = invocation.getArgument(0);

            return entity;
        });
        CurrencyDTO result = currencyService.createCurrency(testData);

        Assertions.assertThat(result.getName()).isEqualTo(testData.getName());
    }

    @Test
    public void givenCurrencyDataWithDuplicateCode_whenCreateCurrency_thenThrowError() {
        CurrencyRequest testData = new CurrencyRequest();
        testData.setName("test");
        when(currencyRepository.existsByCodeAndDeletedFalse(any())).thenReturn(true);

        Assertions.assertThatThrownBy(() -> currencyService.createCurrency(testData))
                .isInstanceOf(ResponseStatusException.class);
    }

}
