#!/bin/sh

set -ex

export branch=$(git symbolic-ref --short HEAD)
export abbrev=$(git rev-parse --short HEAD)

version=$(printf 'VER\t${project.version}' | mvn help:evaluate | grep '^VER' | cut -f2)

TIMESTAMP=$(date +"%Y%m%d")

mvn clean install -Dmaven.test.skip
docker build -t vuongdt23/currency-app:${branch}-${version}-${abbrev}-${TIMESTAMP} .

