

# CRUD APIs @: http://localhost:8080/api/currencies/

# New CurrentPrice API @: http://localhost:8080/api/bpi/currentprice

# Exchange rate is synchronized using a cron job that run on application startup and every hour after that.

# Plus Item: 

## 1. Requests and responses are all logged using filter and interceptor classes.

## 2. Swagger UI @: http://localhost:8080/swagger-ui/index.html

## 4. Used Retry pattern for the Synchronization task (with spring-retry), used Builder from lombok 

## 5 Script for docker image creation @ ./maven_docker_build.sh




